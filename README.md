# Roberto Morati

    Loaded to the database: https://datahub.io/core/geo-countries#python
	API based on ElasticSearch (http://mozio.robertomorati.com)
	Administration: http://mozio.robertomorati.com/admin/login/?next=/admin/
	
    User: admin   password: gvpthdpdr9kpkgkb
	Administration Dashboard allows create Provider and Service Areas.

    This project assumes the use of postgis and elasticsearch on my server.

	Future enhancements:
	   1 - Setup OAuth2.
       2 - Add pagination in ElasticSearch. It's can improve performance with poor hardware. 
       3 - Testing ElasticSearch vs MongoDB
       4 - Use redis to improves Admin with cache. 
       5 - Improve de models to support a User that can create a Company, Provider and Service Area (normalization data).
       6 - Improves documentation of API with Apiary


## 1. API
	
	The API can be used by the website: http://mozio.robertomorati.com/api/v1/
    Documentation: http://mozio.robertomorati.com

## 2. Clone the project

	git clone https://robertomorati@bitbucket.org/robertomorati/moziorm.git

## 3. Attention

    This project takes advantage of my infrastructure.
    To support that was created two containers with external acess allowed: elasticsearch and postgis
    This decision can affect de performance.

## 4. Installation

	docker-compose build --force-rm
	docker-compose up -d

    ###Folder Nginx
        Inside the project was created a docker-compose.yml that support the container mizoram. 
        However, this container nigx is setup to use port 80:80. Any problem, contact me.
        Adjust the nginx to run the project.
        Commands to nginx: 
            docker-compose build --force-rm
            docker-compose up -d 


    
	### Inside the container setup superuser to access admin, follow:
    docker exec -i -t <CONTAINER_ID> /bin/sh
	./manage.py collectstatic
     ### IF SETUP new Database, runs:
    ./manage.py createsuperuser

    ### Also reacreate the index based in the new database
    ./manage.py search_index --rebuild
	