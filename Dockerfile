# Set the base image to use to Ubuntu
FROM python:3.6

MAINTAINER Roberto Morati <robertomorati@gmail.com>

# Local directory with project source
RUN mkdir -p /var/www/moziorm
ENV HOME=/var/www/moziorm


WORKDIR $HOME

# Update the default application repository sources list
RUN apt-get update && apt-get install -y \
        gettext-base \
        python3-venv \
        python3-pip \
        python3-dev

RUN apt-get install -y binutils libproj-dev gdal-bin

RUN pip3 install --upgrade pip
RUN pip install gunicorn
RUN pip install  https://github.com/sabricot/django-elasticsearch-dsl/archive/6.4.1.tar.gz

COPY requirements/requirements.txt $HOME
COPY manage.py $HOME

RUN mkdir media static logs
VOLUME ["$HOME/media/", "$HOME/logs/","$HOME/static/"]

COPY . $HOME

RUN ls
# Install python dependencies
RUN pip install -r $HOME/requirements.txt