# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework import status
from elasticsearch_dsl import connections
from rest_framework.test import APITestCase

from moziorm.core.models import Provider, ServiceArea
from moziorm.api.elastic import ElasticSearchTest


__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'

# elasticsearch to test
elasticsearch_data ={
    'hosts': ['robertomorati.com:9400']
}

connections.create_connection(alias='default',  hosts=elasticsearch_data['hosts'], timeout=20)

coordinates = {
    "type": "Polygon",
    "coordinates": [
        [
            [
                -40.273368,
                -20.256782
            ],
            [
                -40.270236,
                -20.266605
            ],
            [
                -40.263112,
                -20.264351
            ],
            [
                -40.260236,
                -20.261271
            ],
            [
                -40.267017,
                -20.257225
            ],
            [
                -40.269806,
                -20.257446
            ],
            [
                -40.269935,
                -20.256259
            ],
            [
                -40.273368,
                -20.256782
            ]
        ]]}

class ProviderTests(APITestCase):


    def setUp(self):
        super(ProviderTests, self).setUp()

        self.url_list = reverse('api.v1.providers-list')
        self.provider = {
            'name': 'Ifes', 
            'email': 'ifes@ifes.com', 
            'phone_number': '55279999999', 
            'language': 'Portuguese',
            'currency': 'USD'
        }
    

    def test_create_provider(self):
        """
        Ensure create provider
        """

        response = self.client.post(self.url_list, self.provider, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Provider.objects.count(), 1)
        self.assertEqual(Provider.objects.get().name, 'Ifes')


    def test_retrieve_provider(self):
        """
        Ensure retrieve provider
        """
        response = self.client.post(self.url_list, self.provider, format='json')
        instance = Provider.objects.get()

        url = reverse('api.v1.providers-detail', kwargs={'pk': instance.pk})
        response = self.client.patch(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, instance)


class ServiceAreaTests(APITestCase):
    """Testing the ServiceArea API and ElasticSearch"""

    def setUp(self):
        super(ServiceAreaTests, self).setUp()

        self.es = ElasticSearchTest()
        # create data
        p = Provider.objects.create(**{
            'name': 'Ifes', 
            'email': 'ifes@ifes.com', 
            'phone_number': '55279999999', 
            'language': 'Portuguese',
            'currency': 'USD'
        })

        self.data = {
            'name': 'Jardim Camburi Area', 
            'price': 250.0,
            'provider': p.pk,
            'area': coordinates
        }

    def test_create_index(self):
        """
            Ensure recreate the index
        """
        self.es.setup_indices()
        self.assertEqual(self.es.count_indices(), 1) 
    
    def test_create_area(self):
        """
        Ensure create ServiceArea and test signal with ES integration
        """

        url = reverse('api.v1.servicesareas-list')
 
        response = self.client.post(url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_search_index(self):
        """
        Ensure test api search polygon and PURGE the index
        """
        url = reverse('api.v1.servicesareas-get-servicearea', args=(-20.26629, -40.26934))
    
        response = self.client.get(url, format="json")
   
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.es.purge_indices()
        self.assertEqual(self.es.count_indices(), 0)