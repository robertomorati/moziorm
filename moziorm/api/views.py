# -*- coding: utf-8 -*-
from moziorm.core.search import service_area_search, create_query_search
from moziorm.core.models import Provider, ServiceArea
from moziorm.api.serializers import ProviderSerializer, ServiceAreaSerializer

from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status, viewsets, permissions


__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


class ProviderViewSet(viewsets.ModelViewSet):
    """
        ProviderViewSet allows create, update, list, delete and get a Provider
    """
    permission_classes = (permissions.AllowAny,)

    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer


class ServiceAreaViewSet(viewsets.ModelViewSet):
    """
        ServiceAreaViewSet allows create, update, list, delete and get a provider
    """
    permission_classes = (permissions.AllowAny,)

    queryset = ServiceArea.objects.all()
    serializer_class = ServiceAreaSerializer
    
    @action(methods=['GET'], detail=False, url_path='search_area/(-?\d+\.\d+)/(-?\d+\.\d+)', permission_classes=[permissions.AllowAny], name='Retrieves Service Area')
    def get_servicearea(self, request, *args, **kwargs):

        latitude =  args[0]
        longitude =  args[1]
        services_areas = []
        if latitude and longitude:
            query = create_query_search(latitude, longitude)
            response = service_area_search(query)
            for area in response:
                services_areas.append(area.to_dict())
        return Response(services_areas, status=status.HTTP_200_OK)