# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework import routers
from moziorm.api.views import ProviderViewSet, ServiceAreaViewSet

__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'providers', ProviderViewSet, basename='api.v1.providers')
router.register(r'services-areas', ServiceAreaViewSet, basename='api.v1.servicesareas')


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include('rest_framework.urls', namespace='rest_framework'))
]