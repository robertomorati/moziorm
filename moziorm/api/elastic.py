# -*- coding: utf-8 -*-
from django.conf import settings
from django_elasticsearch_dsl.registries import registry


__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


class ElasticSearchTest(object):
    """
    Recreates the Elasticsearch indices every test
    """
    def setup_indices(self):
        for index in registry.get_indices():
            if index.exists():
                index.delete()
            index.create()
    
    def count_indices(self):
        count = 0
        for index in registry.get_indices():
            if index.exists():
                count = count + 1
        return count

    def purge_indices(self):
        for index in registry.get_indices():
            index.delete()

