# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from moziorm.core.models import ServiceArea, Provider

__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


class ProviderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Provider
        fields = ('name', 'email', 'phone_number', 'language', 'currency')


class ServiceAreaSerializer(serializers.ModelSerializer):
    """
        Documentation https://github.com/djangonauts/django-rest-framework-gis
    """
    class Meta:
        model = ServiceArea
        fields = ('name', 'price', 'area', 'provider')