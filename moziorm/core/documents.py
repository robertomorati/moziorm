# -*- coding: utf-8 -*-
from moziorm.core.models import ServiceArea, Provider

from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl.documents import DocType
from django_elasticsearch_dsl.registries import registry

from django.conf import settings
import json


@registry.register_document
class ServiceAreaDocument(DocType):
    """
        ServiceArea DocType
    """
    name = fields.StringField()
    price = fields.FloatField()
    provider = fields.Integer()
    provider_name = fields.StringField()
    area = fields.GeoShapeField()
    
    class Meta:
        doc_type = 'provider_areas-document'

    class Django:
        model = ServiceArea

    class Index:
        name = 'provider_areas-index'
        if settings.TESTING:
            name = 'test_' + name
        doc_type = 'provider_areas-document'

    def prepare_provider_name(self, instance):
        return instance.provider.name

    def prepare_area(self, instance):
        if instance.area:
            return json.loads(instance.area.geojson)
        return []