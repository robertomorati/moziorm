# -*- coding: utf-8 -*-
from django.apps import AppConfig

__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


class MozioRMConfig(AppConfig):
    name = 'moziorm'

    def ready(self):
        import moziorm.core.signals
