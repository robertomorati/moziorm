# -*- coding: utf-8 -*-
from moziorm.core.documents import ServiceAreaDocument
from elasticsearch_dsl import query

def create_query_search(latitude=None,longitude=None):
    """
        Example data latitude=-20.26629&longitude=-40.26934
    """
    if latitude and longitude:

        query_geo_shape = query.Q({'geo_shape': {
            'area': {
                'shape': {
                    'type': 'point',
                    'coordinates': [float(longitude), float(latitude)]
                },
                'relation': 'contains'
            }
        }})
        return query_geo_shape
    return None

def service_area_search(query_search=None,):
    if query_search:
        return ServiceAreaDocument.search().query(query_search)
    return None
