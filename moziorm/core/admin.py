# -*- coding: utf-8 -*-
from django.contrib import admin
from moziorm.core.models import Provider, ServiceArea
from leaflet.admin import LeafletGeoAdmin


__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


class ProviderAdmin(admin.ModelAdmin):
    """
        Custom Admin to ServiceArea
    """
    def get_form(self, request, obj=None, **kwargs):
        return super(ProviderAdmin, self).get_form(request, obj=None, **kwargs)


class ServiceAreaAdmin(LeafletGeoAdmin):
    """
        Custom Admin to ServiceArea
    """
    fields = ['name', 'price', 'provider' , 'area']
    def get_form(self, request, obj=None, **kwargs):
        return super(ServiceAreaAdmin, self).get_form(request, obj=None, **kwargs)


admin.site.register(Provider, ProviderAdmin)
admin.site.register(ServiceArea, ServiceAreaAdmin)