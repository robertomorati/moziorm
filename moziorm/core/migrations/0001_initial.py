# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-07-01 13:52
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('phone_number', models.CharField(max_length=16, verbose_name='Phone Number')),
                ('language', models.CharField(default='English', max_length=255, verbose_name='Language')),
                ('currency', models.CharField(default='USD', max_length=10, verbose_name='Currency')),
            ],
        ),
        migrations.CreateModel(
            name='ServiceArea',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('price', models.FloatField(default=0.0, verbose_name='Price')),
                ('area', django.contrib.gis.db.models.fields.PolygonField(srid=4326, verbose_name='Area')),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='provider_areas', to='core.Provider')),
            ],
        ),
    ]
