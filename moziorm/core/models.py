# -*- coding: utf-8 -*-
from djmoney.models.fields import MoneyField
from django.contrib.gis.db import models as gismodels

import uuid


__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


class Provider(gismodels.Model):
    name = "Provider"
    """
        Provides that represents a company
        Attention: I use User Admin, but, the better solution is
        then applied the data normalization because many users 
        can edit the same provider the represents a Company with many areas.
        After that, OAuth2 solution.
    """
    name = gismodels.CharField(max_length=255, verbose_name='Name')
    email = gismodels.EmailField(verbose_name='Email')
    phone_number = gismodels.CharField(max_length=16, verbose_name='Phone Number')
    language = gismodels.CharField(max_length=255, default='English', verbose_name='Language')
    currency = gismodels.CharField(max_length=10, default='USD', verbose_name='Currency')


    def __str__(self):
        return "%s" % self.name


class ServiceArea(gismodels.Model):
    name = "Service Area"
    """
        Service Area that represents one or more areas of a Provider
    """
    id = gismodels.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    name = gismodels.CharField(max_length=255, verbose_name='Name')
    price = gismodels.FloatField(default=0.0, verbose_name='Price')
    area = gismodels.PolygonField(verbose_name='Area')
    provider = gismodels.ForeignKey(Provider, related_name='provider_areas')

    objects = gismodels.GeoManager()


    def __str__(self):
        return "%s" % self.name



    

