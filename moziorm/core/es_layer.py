# -*- coding: utf-8 -*-
import requests

__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


headers = {
    'Content-Type': 'application/json',
}

class ElasticSearchIntegrate(object):
    """
        ElasticSearchIntegrate provides a simple interface with 
        ElasticSearch to support Polygon type.
        
        This class was necessary because the ElasticSearch DSL(Django) 
        does not provide support to Polygon Type. 

        ABORTED. I decided to persist this file in the project because it's made part of my ideas. 
    """
    
    headers = {
        'Content-Type': 'application/json',
    }

    def create_index(self, index_name):
        pass

    def create(self,obj):
        pass

    def update(self,obj):
        pass

    def remove(self,obj):
        pass

    def search(self,obj):
        pass