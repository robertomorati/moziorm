# -*- coding: utf-8 -*-
from moziorm.core.models import ServiceArea
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

__author__ = 'Roberto Guimaraes Morati Junior <robertomorati@gmail.com>'
__copyright__ = ' Copyright (c) 2019'
__version__ = '0.0.1'


@receiver(post_save, sender=ServiceArea)
def index_post(sender, instance, **kwargs):
    pass


@receiver(post_delete, sender=ServiceArea)
def remove_index_post(sender, instance, **kwargs):
    pass